﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Lifestoned.DataModel.Shared
{
	public interface IChangeLog
    {
        List<ChangelogEntry> Changelog { get; set; }
		string UserChangeSummary { get; set; }
    }

    public class ChangeEntry
    {
        [JsonProperty("userGuid")]
        public string UserGuid { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("submitted")]
        public bool Submitted { get; set; }

        [JsonProperty("submissionTime")]
        public DateTime SubmissionTime { get; set; }

		[JsonIgnore]
		public virtual uint EntryId { get; set; }

		[JsonIgnore]
		public virtual string EntryType { get; }

		[JsonIgnore]
		public virtual IChangeLog ChangeLog { get; }

        public string SubmissionTimeDisplay
        {
            get { return SubmissionTime.ToString("g"); }
        }

		[UIHint("ChangeDiscussionList")]
        [JsonProperty("discussion")]
        public List<ChangeDiscussionEntry> Discussion { get; set; } = new List<ChangeDiscussionEntry>();

        [JsonIgnore]
        public string NewComment { get; set; }

    }

	public abstract class ChangeEntry<T> : ChangeEntry where T : class, IChangeLog
    {
        public override IChangeLog ChangeLog => Entry as IChangeLog;

        public abstract T Entry { get; set; }
    }
}
