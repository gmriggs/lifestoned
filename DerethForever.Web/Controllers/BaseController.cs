/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using Lifestoned.DataModel.Account;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Shared;
using Lifestoned.Providers;

namespace DerethForever.Web.Controllers
{
    public class BaseController : Controller
    {
        private const string _Session_ImportedWeenie = "__importedWeenie";

        private const string _Session_BaseModel = "__baseModel";

        private const string _Session_Account = "__account";

        public Weenie ImportedWeenie
        {
            get { return (Weenie)Session[_Session_ImportedWeenie]; }
            set { Session[_Session_ImportedWeenie] = value; }
        }

        public BaseModel CurrentBaseModel
        {
            get { return (BaseModel)Session[_Session_BaseModel]; }
            set { Session[_Session_BaseModel] = value; }
        }

        /// <summary>
        /// holy hackery, batman!  this is the cross-library glue that holds authentication together.  See JwtCookieManager.cs
        /// </summary>
        public static ApiAccountModel CurrentUser
        {
            get { return (ApiAccountModel)System.Web.HttpContext.Current.Session[_Session_Account]; }
            set { System.Web.HttpContext.Current.Session[_Session_Account] = value; }
        }

        public string GetUserToken()
        {
            var ci = User?.Identity as ClaimsIdentity;
            var claim = ci?.FindFirst("token");

            return claim?.Value;
        }

        public string GetUserName()
        {
            return User.Identity.Name;
        }

        public string GetUserGuid()
        {
            return JwtCookieManager.GetUserGuid(GetUserToken());
        }

        protected ActionResult JsonGet(object value)
        {
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        protected ActionResult JsonGet(object value, bool useJsonNet)
        {
            if (!useJsonNet)
                return Json(value, JsonRequestBehavior.AllowGet);

            JsonNetResult result = new JsonNetResult();
            result.Data = value;
            return result;
        }

        protected ActionResult JsonGet(object value, IContractResolver resolver)
        {
            JsonNetResult result = JsonGet(value, true) as JsonNetResult;
            result.Data = value;
            result.ContractResolver = resolver;
            return result;
        }

        [NonAction]
        protected ActionResult SandboxAction()
        {
            return RedirectToAction("Index", "Sandbox");
        }

        protected string FormatFileName(uint id, string name)
        {
            return FormatFileName(id, name, "json");
        }

        protected string FormatFileName(uint id, string name, string ext)
        {
            foreach (char ifn in System.IO.Path.GetInvalidFileNameChars())
                name = name.Replace(ifn, '_');

            return $"{id} - {name}.{ext}";
        }

        protected ActionResult DownloadJson<T>(T item, uint id, string desc) where T : class
        {
            JsonSerializerSettings s = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            string content = JsonConvert.SerializeObject(item, Formatting.None, s);
            string filename = FormatFileName(id, desc);
            return File(Encoding.UTF8.GetBytes(content), "application/json", filename);
        }
    }

    public class JsonNetResult : JsonResult
    {
        public IContractResolver ContractResolver { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = "application/json";

            if (this.Data != null)
            {
                JsonSerializerSettings s = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
                if (ContractResolver != null)
                    s.ContractResolver = ContractResolver;

                response.Write(JsonConvert.SerializeObject(this.Data, Formatting.None, s));
            }
        }
    }

    public class JsonNetModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            controllerContext.HttpContext.Request.InputStream.Position = 0;
            var stream = controllerContext.RequestContext.HttpContext.Request.InputStream;
            // not disposing this reader is safe since the stream is owned by the request
            // trying to dispose it requires using the leaveOpen constructor
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            string content = reader.ReadToEnd();
            return JsonConvert.DeserializeObject(content, bindingContext.ModelType);
        }
    }

    public class JsonNetIncludePropertyResolver : DefaultContractResolver
    {
        private Dictionary<Type, HashSet<string>> includeProps = new Dictionary<Type, HashSet<string>>();

        public void Include(Type type, params string[] names)
        {
            HashSet<string> included;
            if (!includeProps.TryGetValue(type, out included))
            {
                included = new HashSet<string>();
                includeProps.Add(type, included);
            }

            foreach (string name in names)
                included.Add(name);
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty prop = base.CreateProperty(member, memberSerialization);

            if (includeProps.TryGetValue(prop.DeclaringType, out HashSet<string> included))
            {
                if (!included.Contains(prop.PropertyName))
                {
                    prop.Ignored = true;
                    prop.ShouldSerialize = p => false;
                }
            }

            return prop;
        }
    }
}
