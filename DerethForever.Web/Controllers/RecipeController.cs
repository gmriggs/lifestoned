/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

using log4net;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.Providers;

namespace DerethForever.Web.Controllers
{
    public class RecipeController : BaseController
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpGet]
        public ActionResult Index()
        {
            return View(ContentProviderHost.CurrentRecipeProvider.Get());
        }

        [HttpGet]
        public ActionResult Get(uint id)
        {
            Recipe model = ContentProviderHost.CurrentRecipeProvider.Get(id);
            return JsonGet(model, true);
        }

        [HttpPut]
        [Authorize(Roles = "Admin")]
        public ActionResult Put([ModelBinder(typeof(JsonNetModelBinder))]Recipe model)
        {
            model.LastModified = DateTime.Now;
            model.ModifiedBy = GetUserName();

            try
            {
                ContentProviderHost.CurrentRecipeProvider.Save(model);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                log.Error($"Error Saving Recipe {model.Key} - {model.Description}", ex);
            }

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.InternalServerError, "Save Failed");
        }

        [HttpGet]
        public ActionResult Edit(uint? id)
        {
            return View(id);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(uint id)
        {
            return View(ContentProviderHost.CurrentRecipeProvider.Get(id));
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Recipe model)
        {
            ContentProviderHost.CurrentRecipeProvider.Delete(model.Key);
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult UploadItem()
        {
            string fileNameCopy = "n/a";

            try
            {
                foreach (string fileName in Request.Files)
                {
                    fileNameCopy = fileName;
                    HttpPostedFileBase file = Request.Files[fileName];
                    uint id = 0;

                    using (MemoryStream memStream = new MemoryStream())
                    {
                        file.InputStream.CopyTo(memStream);
                        byte[] data = memStream.ToArray();

                        string serialized = Encoding.UTF8.GetString(data);
                        Recipe item = JsonConvert.DeserializeObject<Recipe>(serialized);
                        id = item.Key;

                        string token = GetUserToken();

                        item.LastModified = DateTime.Now;
                        item.ModifiedBy = GetUserName();

                        // save it
                        ContentProviderHost.CurrentRecipeProvider.Save(item);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error parsing uploaded file {fileNameCopy}.", ex);
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult DownloadOriginal(uint id)
        {
            try
            {
                Recipe model = ContentProviderHost.CurrentRecipeProvider.Get(id);
                return DownloadJson(model, model.Key, model.Description);
            }
            catch (Exception ex)
            {
                log.Error($"Error exporting recipe {id}", ex);
                return RedirectToAction("Index");
            }
        }
    }
}
