/*****************************************************************************************
Copyright 2018 Dereth Forever

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Gdle.Recipes;
using Lifestoned.DataModel.Gdle.Spawns;
using Lifestoned.DataModel.Shared;
using Lifestoned.DataModel.WorldRelease;
using Lifestoned.Providers;

using DerethForever.Web.Models.Discord;

namespace DerethForever.Web.Controllers
{
    using IndexModel = Lifestoned.DataModel.WorldRelease.IndexModel;

    public class WorldReleaseController : BaseController
    {
        private string GetReleaseDir()
        {
            return Path.GetFullPath(ConfigurationManager.AppSettings["WorldReleaseDir"]);
        }

        // GET: Release
        public ActionResult Index(IndexModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                model.Results = new List<Release>();
                string releaseDir = GetReleaseDir();
                var allReleases = Directory.GetFiles(releaseDir, "*.json", SearchOption.AllDirectories).OrderByDescending(f => new FileInfo(f).CreationTime).ToList();

                foreach (var file in allReleases)
                {
                    model.Results.Add(JsonConvert.DeserializeObject<Release>(System.IO.File.ReadAllText(file)));
                }
            }
            catch (Exception ex)
            {
                model.Results = null;
                model.ShowResults = false;
                model.ErrorMessages.Add("Error retrieving data from disk.");
                model.Exception = ex;
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Get(string fileName)
        {
            if (fileName?.Length > 0)
            {
                try
                {
                    string releaseDir = GetReleaseDir();
                    string filePath = Path.GetFullPath(Path.Combine(releaseDir, fileName));
                    return File(filePath, "application/zip", fileName);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return HttpNotFound();
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public ActionResult Cut()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Cut(ReleaseType? type)
        {
            string token = GetUserToken();
            JsonSerializerSettings settings = new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            ZipFile archive = new ZipFile();

            // create a new zip package
            // go through the "providers" creating a folder for each in the zip
            // dump the updates from each provider into its folder

            List<WeenieSearchResult> weenieUpdates = ContentProviderHost.CurrentProvider.AllUpdates(token);
            foreach (WeenieSearchResult result in weenieUpdates)
            {
                Weenie weenie = ContentProviderHost.CurrentProvider.GetWeenie(token, result.WeenieClassId);
                string content = JsonConvert.SerializeObject(weenie, settings);

                archive.AddFile($"weenies/{FormatFileName(weenie.WeenieClassId, weenie.Name)}", content);
            }

            foreach (SpawnMapEntry map in ContentProviderHost.CurrentProvider.GetLandblocks())
            {
                string content = JsonConvert.SerializeObject(map, settings);
                archive.AddFile($"spawnMaps/{FormatFileName(map.Key, map.Description)}", content);
            }

            foreach (Recipe recipe in ContentProviderHost.CurrentRecipeProvider.Get())
            {
                string content = JsonConvert.SerializeObject(recipe, settings);
                archive.AddFile($"recipes/{FormatFileName(recipe.Key, recipe.Description)}", content);
            }

            string releaseDir = GetReleaseDir();
            string fileName = $"GDLE-Latest-Updates-{DateTime.Now:yyyy-MM-dd_hh-mm}.zip";
            string filePath = Path.GetFullPath(Path.Combine(releaseDir, fileName));

            using (Stream package = archive.BuildZipStream())
            using (FileStream fs = System.IO.File.OpenWrite(Path.Combine(filePath)))
            {
                package.Seek(0, SeekOrigin.Begin);
                package.CopyTo(fs);
            }

            Release release = new Release();
            release.FileName = fileName;
            release.Type = ReleaseType.Partial;
            release.ReleaseDateTime = DateTime.Now.ToString("yyyy-MM-dd");

            filePath = Path.GetFullPath(Path.Combine(releaseDir, $"{DateTime.Now.ToString("yyyy-MM-dd_hhmmss")}.json"));

            System.IO.File.WriteAllText(filePath, JsonConvert.SerializeObject(release, settings));

            WorldReleaseEvent wre = new WorldReleaseEvent();
            wre.User = CurrentUser.DisplayName;
            wre.ReleaseTime = DateTimeOffset.Now;
            wre.Name = release.FileName;
            wre.Size = release.FileSize;
            DiscordController.PostToDiscordAsync(wre);

            return RedirectToAction("Index", "Server");
        }
    }
}
