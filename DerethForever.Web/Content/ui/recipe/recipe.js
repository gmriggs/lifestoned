﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-recipe', {
    props: ['recipeId'],
    data() {
        return {
        };
    },
    computed: {
        recipe: {
            get() { return this.$recipe.recipe; },
            set(val) { this.$recipe.recipe = val; }
        },
        isNew() {
            return this.$recipe.isNew;
        }
    },
    created() {
    },
    methods: {

    },
    template: `
    <div>
        <div class="well">
            <div class="row">
                <div class="col-md-7">
                    <input v-model.number="recipe.key" type="text" class="form-control" placeholder="Recipe Id" />
                    <br />
                    <input v-model="recipe.desc" type="text" class="form-control" placeholder="Description" />
                </div>

                <div v-if="!isNew" class="col-md-4">
                    <div class="row row-spacer">
                        <div class="col-md-6 strong text-right">Modified By</div>
                        <div class="col-md-6">{{ recipe.ModifiedBy }}</div>
                    </div>
                    <div class="row row-spacer">
                        <div class="col-md-6 strong text-right">Last Modified</div>
                        <div class="col-md-6">{{ recipe.LastModified | toDate }}</div>
                    </div>
                    <div class="row row-spacer">
                        <div class="col-md-6 strong text-right">Is Final</div>
                        <div class="col-md-6"><input type="checkbox" id="is_done" v-model="recipe.isDone" /></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">

        <div id="general" class="tab-pane fade in active">
            <lsd-panel title="General">

                <div class="row row-spacer">
                    <div class="col-md-3">Skill</div>
                    <div class="col-md-3">Difficulty</div>
                    <div class="col-md-3">Check Formula</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><lsd-enum-select v-model="recipe.recipe.Skill" type="SkillId" keyOnly></lsd-enum-select></div>
                    <div class="col-md-3"><input v-model="recipe.recipe.Difficulty" type="number" min="0" class="form-control" /></div>
                    <div class="col-md-3"><lsd-enum-select v-model="recipe.recipe.SkillCheckFormulaType" type="RecipeSkillCheck" keyOnly></lsd-enum-select></div>
                </div>

                <div class="row row-spacer">
                    <div class="col-md-3">Data Id</div>
                    <div class="col-md-3">Unknown</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><input v-model="recipe.recipe.DataID" type="number" class="form-control" /></div>
                    <div class="col-md-3"><input v-model="recipe.recipe.Unknown" type="number" class="form-control" /></div>
                </div>

                <hr />

                <div class="row row-spacer">
                    <div class="col-md-3">Success Wcid</div>
                    <div class="col-md-2">Amount</div>
                    <div class="col-md-4">Message</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><input v-lsd-weenie-finder v-model="recipe.recipe.SuccessWcid" type="text" class="form-control" /></div>
                    <div class="col-md-2"><input v-model="recipe.recipe.SuccessAmount" type="number" min="0" class="form-control" /></div>
                    <div class="col-md-4"><input v-model="recipe.recipe.SuccessMessage" type="text" class="form-control" /></div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3">Tool Amount</div>
                    <div class="col-md-2">Chance</div>
                    <div class="col-md-4">Message</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><input v-model="recipe.recipe.SuccessConsumeToolAmount" type="number" min="0" class="form-control" /></div>
                    <div class="col-md-2"><input v-model="recipe.recipe.SuccessConsumeToolChance" type="number" min="0" max="1" step="0.00001" class="form-control" /></div>
                    <div class="col-md-4"><input v-model="recipe.recipe.SuccessConsumeToolMessage" type="text" class="form-control" /></div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3">Target Amount</div>
                    <div class="col-md-2">Chance</div>
                    <div class="col-md-4">Message</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><input v-model="recipe.recipe.SuccessConsumeTargetAmount" type="number" min="0" class="form-control" /></div>
                    <div class="col-md-2"><input v-model="recipe.recipe.SuccessConsumeTargetChance" type="number" min="0" max="1" step="0.00001" class="form-control" /></div>
                    <div class="col-md-4"><input v-model="recipe.recipe.SuccessConsumeTargetMessage" type="text" class="form-control" /></div>
                </div>

                <hr />

                <div class="row row-spacer">
                    <div class="col-md-3">Failure Wcid</div>
                    <div class="col-md-2">Amount</div>
                    <div class="col-md-4">Message</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><input v-lsd-weenie-finder v-model="recipe.recipe.FailWcid" type="text" class="form-control" /></div>
                    <div class="col-md-2"><input v-model="recipe.recipe.FailAmount" type="number" min="0" class="form-control" /></div>
                    <div class="col-md-4"><input v-model="recipe.recipe.FailMessage" type="text" class="form-control" /></div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3">Tool Amount</div>
                    <div class="col-md-2">Chance</div>
                    <div class="col-md-4">Message</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><input v-model="recipe.recipe.FailureConsumeToolAmount" type="number" min="0" class="form-control" /></div>
                    <div class="col-md-2"><input v-model="recipe.recipe.FailureConsumeToolChance" type="number" min="0" max="1" step="0.00001" class="form-control" /></div>
                    <div class="col-md-4"><input v-model="recipe.recipe.FailureConsumeToolMessage" type="text" class="form-control" /></div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3">Target Amount</div>
                    <div class="col-md-2">Chance</div>
                    <div class="col-md-4">Message</div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3"><input v-model="recipe.recipe.FailureConsumeTargetAmount" type="number" min="0" class="form-control" /></div>
                    <div class="col-md-2"><input v-model="recipe.recipe.FailureConsumeTargetChance" type="number" min="0" max="1" step="0.00001" class="form-control" /></div>
                    <div class="col-md-4"><input v-model="recipe.recipe.FailureConsumeTargetMessage" type="text" class="form-control" /></div>
                </div>

            </lsd-panel>

        </div>

        <div id="reqs" class="tab-pane fade">
            <lsd-recipe-req-set v-model="recipe.recipe.Requirements" title="Requirements"></lsd-recipe-req-set>
        </div>

        <div id="mods" class="tab-pane fade">
            <lsd-recipe-mod-set v-model="recipe.recipe.Mods" title="Success Mods" start="0"></lsd-recipe-mod-set>

            <lsd-recipe-mod-set v-model="recipe.recipe.Mods" title="Failure Mods" start="4"></lsd-recipe-mod-set>
        </div>

        <div id="precursors" class="tab-pane fade">
            <lsd-recipe-precursors v-model="recipe.precursors"></lsd-recipe-precursors>
        </div>

        <div id="history" class="tab-pane fade">
            <lsd-changelog v-model="recipe"></lsd-changelog>
        </div>

        </div>

    </div>
    `
});

Vue.component('lsd-recipe-precursors', {
    props: ['items'],
    model: { prop: 'items', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    created() {
        mapWatchers(this, 'items', 'changed');
    },
    methods: {
        addNew() {
            this.items.push({
                'tool': null,
                'target': null
            });
        },
        deleting(idx) {
            this.items.splice(idx, 1);
        }
    },
    template: `
    <lsd-panel title="Precursors" stickHeader showAdd @adding="addNew">
        <div class="row row-spacer">
            <div class="col-md-5">Tool</div>
            <div class="col-md-5">Target</div>
            <div class="col-md-1 col-md-offset-1"></div>
        </div>

        <div v-for="(item, idx) in items" class="row row-spacer">
            <div class="col-md-5"><input v-lsd-weenie-finder v-model="item.tool" type="text" required class="form-control" /></div>
            <div class="col-md-5"><input v-lsd-weenie-finder v-model="item.target" type="text" required class="form-control" /></div>
            <div class="col-md-1 col-md-offset-1"><button @click="deleting(idx)" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button></div>
        </div>
    </lsd-panel>
    `
});

Vue.component('lsd-recipe-req-set', {
    props: ['items', 'title'],
    model: { prop: 'items', event: 'changed' },
    data() {
        return {
            current: 0
        };
    },
    computed: {
        reqs: {
            get() {
                return this.items && this.items[this.current] ? this.items[this.current] : null;
            },
            set(val) {
                this.items.splice(this.current, 1, val);
            }
        }
    },
    created() {
        mapWatchers(this, 'items', 'changed');
    },
    mounted() {
    },
    methods: {
        setReq(idx) {
            this.current = idx;
        }
    },
    template: `
    <lsd-panel :title="title">
        <ul class="nav nav-pills border-after">
            <li class="active"><a @click="setReq(0)" data-toggle="pill">Target</a></li>
            <li><a @click="setReq(1)" data-toggle="pill">Tool</a></li>
            <li><a @click="setReq(2)" data-toggle="pill">Player</a></li>
        </ul>
        <div>
            <lsd-recipe-req v-model="reqs"></lsd-recipe-req>
        </div>
    </lsd-panel>
    `
});

Vue.component('lsd-recipe-req', {
    props: ['item', 'title'],
    model: { prop: 'item', event: 'changed' },
    data() {
        return {
            current: 'IntRequirements',
            currentStat: 'IntPropertyId'
        };
    },
    computed: {
        requirements() {
            return this.item && this.item[this.current] ? this.item[this.current] : [];
        }
    },
    created() {
        mapWatchers(this, 'item', 'changed');
    },
    methods: {
        newItem() {
            var item = {
                IntRequirements: [],
                DIDRequirements: [],
                IIDRequirements: [],
                FloatRequirements: [],
                StringRequirements: [],
                BoolRequirements: []
            };
            this.$emit('changed', item);
        },
        setReq(prop, type) {
            this.current = prop;
            this.currentStat = type;
        },
        addNew() {
            var item = this.item;

            item[this.current].push({
                'Stat': null,
                'Value': null,
                'OperationType': null,
                'Message': null
            });
            this.$emit('changed', item);
        },
        deleted(idx) {
            this.requirements.splice(idx, 1);
        }
    },
    template: `
    <div class="panel-body">
        <div v-if="!item" class="row row-spacer">
            <div class="col-md-4 col-md-offset-4"><button @click="newItem" type="button" class="btn btn-lg btn-default">Create Entry</button></div>
        </div>
        <div v-else>
        <ul class="nav nav-pills border-after">
            <li class="active"><a @click="setReq('IntRequirements', 'IntPropertyId')" data-toggle="pill">Ints</a></li>
            <li><a @click="setReq('DIDRequirements', 'DidPropertyId')" data-toggle="pill">DIDs</a></li>
            <li><a @click="setReq('IIDRequirements', 'IidPropertyId')" data-toggle="pill">IIDs</a></li>
            <li><a @click="setReq('FloatRequirements', 'DoublePropertyId')" data-toggle="pill"Floats</a></li>
            <li><a @click="setReq('StringRequirements', 'StringPropertyId')" data-toggle="pill">Strings</a></li>
            <li><a @click="setReq('BoolRequirements', 'BoolPropertyId')" data-toggle="pill">Bools</a></li>

            <li class="pull-right"><button @click="addNew" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button></li>
        </ul>
        <div>
            <lsd-recipe-req-stats v-model="requirements" :stat-type="currentStat" @deleted="deleted"></lsd-recipe-req-stats>
        </div>
        </div>
    </div>
    `
});

Vue.component('lsd-recipe-req-stats', {
    props: ['items', 'statType'],
    model: { prop: 'items', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    created() {
        mapWatchers(this, 'items', 'changed');
    },
    methods: {

    },
    template: `
    <div class="tab-pane">
        <div class="row row-spacer">
            <div class="col-md-3">Stat</div>
            <div class="col-md-2">Value</div>
            <div class="col-md-2">Op</div>
            <div class="col-md-4">Message</div>
            <div class="col-md-1"></div>
        </div>

        <div v-for="(item, idx) in items" class="row row-spacer">
            <div class="col-md-3"><lsd-enum-select v-model="item.Stat" :type="statType"></lsd-enum-select></div>
            <div class="col-md-2"><input v-model="item.Value" type="number" class="form-control" /></div>
            <div class="col-md-2"><lsd-enum-select v-model="item.OperationType" type="StatReqOperation"></lsd-enum-select></div>
            <div class="col-md-4"><input v-model="item.Message" type="text" class="form-control" /></div>
            <div class="col-md-1"><button @click="$emit('deleted', idx)" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button></div>
        </div>
    </div>
    `
});

Vue.component('lsd-recipe-mod-set', {
    props: ['items', 'title', 'start'],
    model: { prop: 'items', event: 'changed' },
    data() {
        return {
            current: parseInt(this.start)
        };
    },
    computed: {
        mods: {
            get() {
                return this.items && this.items[this.current] ? this.items[this.current] : null;
            },
            set(val) {
                this.items.splice(this.current, 1, val);
            }
        }
    },
    created() {
        mapWatchers(this, 'items', 'changed');
    },
    mounted() {
    },
    methods: {
        setMod(idx) {
            this.current = parseInt(this.start) + idx;
        }
    },
    template: `
    <lsd-panel :title="title">
        <ul class="nav nav-pills border-after">
            <li class="active"><a @click="setMod(0)" data-toggle="pill">Target</a></li>
            <li><a @click="setMod(1)" data-toggle="pill">Tool</a></li>
            <li><a @click="setMod(2)" data-toggle="pill">Player</a></li>
            <li><a @click="setMod(3)" data-toggle="pill">Created</a></li>
        </ul>
        <div>
            <lsd-recipe-mod v-model="mods"></lsd-recipe-mod>
        </div>
    </lsd-panel>
    `
});

Vue.component('lsd-recipe-mod', {
    props: ['item'],
    model: { prop: 'item', event: 'changed' },
    data() {
        return {
            current: 'IntRequirements',
            currentStat: 'IntPropertyId'
        };
    },
    computed: {
        requirements() {
            return this.item && this.item[this.current] ? this.item[this.current] : [];
        }
    },
    created() {
        mapWatchers(this, 'item', 'changed');
    },
    mounted() {
    },
    methods: {
        newItem() {
            var item = {
                ModifyHealth: null,
                ModifyStamina: null,
                ModifyMana: null,
                RequiresHealth: null,
                RequiresStamina: null,
                RequiresMana: null,
                ModificationScriptId: null,
                Unknown7: null,
                Unknown9: null,
                Unknown10: null,
                IntRequirements: [],
                DIDRequirements: [],
                IIDRequirements: [],
                FloatRequirements: [],
                StringRequirements: [],
                BoolRequirements: []
            };
            this.$emit('changed', item);
        },
        setReq(prop, type) {
            this.current = prop;
            this.currentStat = type;
        },
        addNew() {
            var reqs = this.item;
            reqs[this.current].push({
                'Stat': null,
                'Value': null,
                'OperationType': null,
                'Unknown': null
            });
            this.$emit('changed', reqs);
        },
        deleted(idx) {
            this.requirements.splice(idx, 1);
        }
    },
    template: `
    <div class="panel-body">
        <div v-if="!item" class="row row-spacer">
            <div class="col-md-4 col-md-offset-4"><button @click="newItem" type="button" class="btn btn-lg btn-default">Create Entry</button></div>
        </div>
        <div v-else>
        <div class="row row-spacer">
            <div class="col-md-3">Modify Health</div>
            <div class="col-md-3">Stamina</div>
            <div class="col-md-3">Mana</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3"><input v-model="item.ModifyHealth" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="item.ModifyStamina" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="item.ModifyMana" type="number" class="form-control" /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3">Requires Health</div>
            <div class="col-md-3">Stamina</div>
            <div class="col-md-3">Mana</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3"><input v-model="item.RequiresHealth" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="item.RequiresStamina" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="item.RequiresMana" type="number" class="form-control" /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3">Mutation Script</div>
            <div class="col-md-3">Unknown7</div>
            <div class="col-md-3">Unknown9</div>
            <div class="col-md-3">Unknown10</div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3"><input v-model="item.ModificationScriptId" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="item.Unknown7" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="item.Unknown9" type="number" class="form-control" /></div>
            <div class="col-md-3"><input v-model="item.Unknown10" type="number" class="form-control" /></div>
        </div>

        <ul class="nav nav-pills border-after">
            <li class="active"><a @click="setReq('IntRequirements', 'IntPropertyId')" data-toggle="pill">Ints</a></li>
            <li><a @click="setReq('DIDRequirements', 'DidPropertyId')" data-toggle="pill">DIDs</a></li>
            <li><a @click="setReq('IIDRequirements', 'IidPropertyId')" data-toggle="pill">IIDs</a></li>
            <li><a @click="setReq('FloatRequirements', 'DoublePropertyId')" data-toggle="pill">Floats</a></li>
            <li><a @click="setReq('StringRequirements', 'StringPropertyId')" data-toggle="pill">Strings</a></li>
            <li><a @click="setReq('BoolRequirements', 'BoolPropertyId')" data-toggle="pill">Bools</a></li>

            <li class="pull-right"><button @click="addNew" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button></li>
        </ul>
        <div>
            <lsd-recipe-mod-stats v-model="requirements" :stat-type="currentStat" @deleted="deleted"></lsd-recipe-mod-stats>
        </div>
        </div>
    </div>
    `
});

Vue.component('lsd-recipe-mod-stats', {
    props: ['items', 'statType'],
    model: { prop: 'items', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
    },
    created() {
        mapWatchers(this, 'items', 'changed');
    },
    methods: {

    },
    template: `
    <div class="tab-pane">
        <div class="row row-spacer">
            <div class="col-md-4">Stat</div>
            <div class="col-md-2">Value</div>
            <div class="col-md-3">Op</div>
            <div class="col-md-2">Unknown</div>
            <div class="col-md-1"></div>
        </div>

        <div v-for="(item, idx) in items" class="row row-spacer">
            <div class="col-md-4"><lsd-enum-select v-model="item.Stat" :type="statType"></lsd-enum-select></div>
            <div class="col-md-2"><input v-model="item.Value" type="number" class="form-control" /></div>
            <div class="col-md-3"><lsd-enum-select v-model="item.OperationType" type="StatModOperation"></lsd-enum-select></div>
            <div class="col-md-2"><input v-model="item.Unknown" type="number" class="form-control" /></div>
            <div class="col-md-1"><button @click="$emit('deleted', idx)" type="button" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button></div>
        </div>
    </div>
    `
});
