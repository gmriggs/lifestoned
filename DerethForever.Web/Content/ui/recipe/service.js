﻿
/// <reference path="../../../Scripts/vue.js" />

function makeRecipeService(id) {
    return new Vue({
        data() {
            return {
                recipeId: id,
                recipe: {
                    key: null,
                    desc: null,
                    recipe: {
                        Mods: [null, null, null, null, null, null, null, null],
                        Requirements: [null, null, null]
                    },
                    precursors: []
                },
                isNew: true
            };
        },
        watch: {
            'recipe.key': function (val, old) {
                this.recipe.recipe.RecipeID = val;
            }
        },
        methods: {
            fetch(id) {
                var $this = this;
                $this.recipeId = id;
                $.getJSON("/Recipe/Get", { id: $this.recipeId },
                    function (res) {
                        $this.recipe = res;
                        $this.isNew = false;
                    });
            },
            save() {
                $.ajax({
                    type: "PUT",
                    url: "/Recipe/Put",
                    data: JSON.stringify(this.recipe),
                    contentType: 'application/json',
                    processData: false,
                    success: function (data, status, xhr) {
                        alert('Save Successful');
                        console.log(data, status);
                    },
                    error: function (xhr, status, err) {
                        alert('Save Failed');
                        console.error(status, err);
                    }
                });
            }

        },
        created() {
            if (this.recipeId !== undefined) {
                this.fetch(this.recipeId);
            }
        }
    });
}

const RecipeStore = {
    install(Vue, options) {
        Vue.mixin({
            beforeCreate() {
                const opts = this.$options;
                if (opts.recipeStore) {
                    this.$recipe = opts.recipeStore;
                } else if (opts.parent && opts.parent.$recipe) {
                    this.$recipe = opts.parent.$recipe;
                }
            }
        });
    }
};
