﻿
/// <reference path="../../../Scripts/vue.js" />

function makeWeenieService(id, clone, userId) {
    return new Vue({
        data() {
            return {
                weenieId: id,
                weenie: {
                    WeenieClassId: null,
                    WeenieTypeId: null,
                    Name: null,
                    IconId: null,
                    ModifiedBy: null,
                    LastModified: null,
                    IsDone: false,
                    StringStats: [],
                    IntStats: [],
                    Int64Stats: [],
                    FloatStats: [],
                    IidStats: [],
                    DidStats: [],
                    BoolStats: [],
                    Positions: []
                },
                isNew: true,
                isClone: clone,
                userId
            };
        },
        watch: {
            'weenie.WeenieTypeId': function (val, old) {
                this.weenie.WeenieType_Binder = val;
            },
            'weenie.Name': function (val, old) {
                // verify the string prop exists
                var name = this.weenie.StringStats.find(function (stat) {
                    if (stat.Key === 1)
                        return stat;
                });

                if (!name) {
                    name = {
                        Key: 1,
                        PropertyIdBinder: '01 - Name',
                        Value: ''
                    };
                    this.weenie.StringStats.push(name);
                }
                name.Value = val;
            },
            'weenie.IntStats': {
                deep: true,
                handler(val, old) {
                    var item = this.weenie.IntStats.find((v) => v.Key === 1);
                    if (item) this.weenie.ItemType = item.Value;
                }
            }
        },
        methods: {
            fetch(id) {
                var $this = this;
                $this.weenieId = id;
                $.getJSON("/Weenie/Get", { id: $this.weenieId, userGuid: $this.userId },
                    function (res) {
                        if (!res.Positions) res.Positions = [];
                        $this.weenie = Object.assign({}, $this.weenie, res);

                        if ($this.isClone) {
                            $this.weenieId = null;
                            $this.weenie.WeenieClassId = null;
                            $this.weenie.LastModified = null;
                            $this.weenie.Changelog = [];
                        } else {
                            $this.isNew = false;
                        }
                    });
            },
            save() {
                $.ajax({
                    type: this.isNew ? "POST" : "PUT",
                    url: this.isNew ? "/Weenie/New" : "/Weenie/Put",
                    //data: this.weenie,
                    data: JSON.stringify(this.weenie),
                    contentType: 'application/json',
                    //dataType: 'json',
                    success: function (data, status, xhr) {
                        console.log(data, status);
                        alert('Saved Successfully');
                        //window.location.href = '/Sandbox';
                    },
                    error: function (xhr, status, err) {
                        console.error(status, err);
                        alert('Save Failed');
                    }
                });
            },

            validate() {
                // item type is required
                if (this.weenie.IntStats.find((o) => o.Key === 1) === null)
                    return false;

                return true;
            },

            newFrame() {
                return {
                    Display: null//'[0.000000 0.000000 2.000000] 1.000000 0.000000 0.000000 0.000000'
                };
            },
            newPosition() {
                return {
                    Display: null//'0x00000000 [0.000000 0.000000 2.000000] 1.000000 0.000000 0.000000 0.000000'
                };
            },

            newBodyPart() {
                return {
                    DType: null,
                    DVal: null,
                    DVar: null,
                    BH: null,
                    ArmorValues: {
                        BaseArmor: null,
                        ArmorVsBludgeon: null,
                        ArmorVsPierce: null,
                        ArmorVsSlash: null,
                        ArmorVsAcid: null,
                        ArmorVsCold: null,
                        ArmorVsFire: null,
                        ArmorVsElectric: null,
                        ArmorVsNether: null
                    },
                    SD: {
                        HLF: null,
                        MLF: null,
                        LLF: null,
                        HRF: null,
                        MRF: null,
                        LRF: null,
                        HLB: null,
                        MLB: null,
                        LLB: null,
                        HRB: null,
                        MRB: null,
                        LRB: null
                    }
                };
            },

            newCreateListEntry() {
                return {
                    WeenieClassId: null,
                    Palette: null,
                    Shade: null,
                    Destination: null,
                    Destination_Binder: null,
                    StackSize: null,
                    TryToBond: null,
                    TryToBond_BooleanBinder: null,
                    Deleted: false
                };
            },

            newEmote(categoryId) {
                return {
                    Actions: [],
                    Category: categoryId,
                    EmoteCategory: categoryId,
                    Deleted: false,
                    ClassId: null,
                    MaxHealth: null,
                    MinHealth: null,
                    NewEmoteType: null,
                    Probability: null,
                    Quest: null,
                    SortOrder: null,
                    Style: null,
                    SubStyle: null,
                    VendorType: null
                };
            },
            newEmoteCategory(categoryId) {
                return {
                    EmoteCategoryId: categoryId,
                    Emotes: [this.newEmote(categoryId)],
                    Deleted: false
                };
            },
            newEmoteAction(actionType) {
                return {
                    EmoteActionType: actionType,
                    EmoteActionType_Binder: actionType,
                    Delay: 0,
                    Extent: 1,
                    Amount: null,
                    Motion: null,
                    Message: null,
                    Amount64: null,
                    HeroXp64: null,
                    Item: null,
                    Minimum64: null,
                    Maximum64: null,
                    Percent: null,
                    Display_Binder: null,
                    Display: null,
                    Max: null,
                    Min: null,
                    FMax: null,
                    FMin: null,
                    Stat: null,
                    PScript: null,
                    PScript_Binder: null,
                    Sound: null,
                    MPosition: this.newPosition(),
                    Frame: this.newFrame(),
                    SpellId: null,
                    TestString: null,
                    WealthRating: null,
                    TreasureClass: null,
                    TreasureType: null,
                    SortOrder: null,
                    Deleted: false
                };
            },

            newGeneratorListEntry() {
                return {
                    Delay: null,
                    Frame: this.newFrame(),
                    InitCreate: null,
                    MaxNumber: null,
                    ObjectCell: null,
                    Probability: null,
                    PaletteId: null,
                    Shade: null,
                    Slot: null,
                    StackSize: null,
                    WeenieClassId: null,
                    WhenCreate: null,
                    WhereCreate: null,
                    WhenCreateEnum: null,
                    WhereCreateEnum: null,
                    Deleted: false
                };
            }

        },
        created() {
            if (this.weenieId !== undefined && this.weenieId !== 0) {
                this.fetch(this.weenieId);
            }
        }
    });
}

const WeenieStore = {
    install(Vue, options) {
        Vue.mixin({
            beforeCreate() {
                const opts = this.$options;
                if (opts.weenieStore) {
                    this.$weenie = opts.weenieStore;
                } else if (opts.parent && opts.parent.$weenie) {
                    this.$weenie = opts.parent.$weenie;
                }
            }
        });
    }
};
