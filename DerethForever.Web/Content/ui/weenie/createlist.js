﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-create-items', {
    props: ['items'],
    model: { prop: 'items', event: 'changed' },
    methods: {
        nodeMoved(evt) {
            var n = evt.el;
            var nn = evt.next;

            var current = -1;
            var before = -1;

            if (n && n.item) {
                current = this.items.indexOf(n.item);
            }

            if (nn && nn.item) {
                before = this.items.indexOf(nn.item);
            }

            if (current < before) before--;
            var item = this.items.splice(current, 1);
            this.items.splice(before, 0, ...item);

            this.$emit('changed', this.items);
        },
        deleted(item) {
            var idx = this.items.indexOf(item);
            if (idx >= 0) {
                this.items.splice(idx, 1);
            }
        },
        addNew() {
            if (!this.items) this.items = [];
            this.items.push(this.$weenie.newCreateListEntry());
        }
    },
    template: `
    <lsd-panel title="Create List" stickHeader showAdd @adding="addNew">
        <lsd-create-items-header showDelete></lsd-create-items-header>
        <div v-lsd-drag-sort="'div.row'" @moved="nodeMoved($event)" class="panel-group">
        <lsd-create-items-entry v-for="(item, idx) in items" :key="$hash(item)" v-model="items[idx]" showDelete @deleted="deleted"></lsd-create-items-entry>
        </div>
        <!-- footer -->
    </lsd-panel>
    `
});

Vue.component('lsd-create-items-header', {
    props: {
        showDelete: { type: Boolean, default: false }
    },
    template: `
    <div class="row row-spacer">
        <div class="col-md-3">Weenie</div>
        <div class="col-md-3">Destination</div>
        <div class="col-md-1">Palette</div>
        <div class="col-md-2">Shade</div>
        <div class="col-md-1">Quantity</div>
        <div class="col-md-1">Bonded</div>
        <div v-if="showDelete" class="col-md-1"></div>
    </div>
    `
});

Vue.component('lsd-create-items-entry', {
    props: {
        'item': { type: Object },
        showDelete: { type: Boolean, default: false }
    },
    model: { prop: 'item', event: 'changed' },
    methods: {
        deleted() {
            this.$emit('deleted', this.item);
        }
    },
    created() {
        if (this.item === null) this.$emit('changed', this.$weenie.newCreateListEntry());
    },
    template: `
    <div class="row row-spacer">
        <div class="col-md-3">
            <input v-lsd-weenie-finder v-model="item.WeenieClassId" type="text" class="form-control" placeholder="Weenie" />
        </div>
        <div class="col-md-3">
            <lsd-enum-select type="Destination" v-model="item.Destination" keyOnly></lsd-enum-select>
        </div>
        <div class="col-md-1">
            <input v-model="item.Palette" type="text" class="form-control" />
        </div>
        <div class="col-md-2">
            <input v-model="item.Shade" type="text" class="form-control" />
        </div>
        <div class="col-md-1">
            <input v-model="item.StackSize" type="text" class="form-control" />
        </div>
        <div class="col-md-1">
            <input v-model="item.TryToBond_BooleanBinder" type="checkbox" class="form-control" />
        </div>
        <div v-if="showDelete" class="col-md-1">
            <button @click="deleted" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
        </div>
    </div>
    `
});
