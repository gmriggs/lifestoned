﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-weenie', {
    props: ['weenie'],
    model: { prop: 'weenie', event: 'changed' },
    data() {
        return {
        };
    },
    computed: {
        /*weenie: {
            get() { return this.$weenie.weenie; },
            set(val) { this.$weenie.weenie = val; }
        },*/
        iconId() {
            var item = this.weenie.DidStats.find((v) => v.Key === 8);
            return item ? item.Value : 0;
        },
        uiEffects() {
            var item = this.weenie.IntStats.find((v) => v.Key === 18);
            return item ? item.Value : 0;
        },
        iconUrl() {
            return `/Resource/GetDynamicIcon?itemType=${this.weenie.ItemType}&iconId=${this.iconId}&uiEffects=${this.uiEffects}`;
        },
        isNew() {
            return this.$weenie.isNew;
        }
    },
    created() {
    },
    methods: {

    },
    template: `
    <div>
        <div class="well">
            <div v-if="isNew">
                <div class="row row-spacer">
                    <div class="col-md-3">Weenie Name</div>
                    <div class="col-md-8"><input v-model="weenie.Name" type="text" class="form-control" placeholder="Name" required /></div>
                </div>
                <div class="row row-spacer">
                    <div class="col-md-3">Weenie Id</div>
                    <div class="col-md-8"><input v-model.number="weenie.WeenieClassId" type="number" class="form-control" placeholder="Weenie Id" required /></div>
                </div>
            </div>
            <div v-else class="row">
                <div class="col-md-1 weenie-icon">
                    <img v-if="weenie.IconId" :src="iconUrl" />
                </div>
                <div class="col-md-7">
                    <h2>{{ weenie.Name }} ({{ weenie.WeenieClassId }})</h2>
                </div>
                <div v-if="!isNew" class="col-md-4">
                    <div class="row row-spacer">
                        <div class="col-md-6 strong text-right">Modified By</div>
                        <div class="col-md-6">{{ weenie.ModifiedBy }}</div>
                    </div>
                    <div class="row row-spacer">
                        <div class="col-md-6 strong text-right">Last Modified</div>
                        <div class="col-md-6">{{ weenie.LastModified | toDate }}</div>
                    </div>
                    <div class="row row-spacer">
                        <div class="col-md-6 strong text-right">Is Final</div>
                        <div class="col-md-6"><input v-model="weenie.IsDone" type="checkbox" /></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-content">

        <div id="general" class="tab-pane fade in active">
            <div class="well">
                <div class="row row-spacer">
                    <div class="col-md-3">Weenie Type</div>
                    <div class="col-md-8">
                    <lsd-enum-select type="WeenieType" v-model="weenie.WeenieTypeId" keyOnly required></lsd-enum-select>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Properties</h3>

                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="tab" href="#stringProperties">String<span class="badge">{{ weenie.StringStats ? weenie.StringStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#int32Properties">Int32<span class="badge">{{ weenie.IntStats ? weenie.IntStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#int64Properties">Int64<span class="badge">{{ weenie.Int64Stats ? weenie.Int64Stats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#dblProperties">Float<span class="badge">{{ weenie.FloatStats ? weenie.FloatStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#didProperties">Data ID<span class="badge">{{ weenie.DidStats ? weenie.DidStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#iidProperties">Instance ID<span class="badge">{{ weenie.IidStats ? weenie.IidStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#boolProperties">Bool<span class="badge">{{ weenie.BoolStats ? weenie.BoolStats.length : 0 }}</span></a></li>
                        <li><a data-toggle="tab" href="#positions">Position<span class="badge">{{ weenie.Positions ? weenie.Positions.length : 0 }}</span></a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <lsd-string-props id="stringProperties" v-model="weenie.StringStats" :active="true"></lsd-string-props>

                    <lsd-int-props id="int32Properties" v-model="weenie.IntStats"></lsd-int-props>

                    <lsd-long-props id="int64Properties" v-model="weenie.Int64Stats"></lsd-long-props>

                    <lsd-double-props id="dblProperties" v-model="weenie.FloatStats"></lsd-double-props>

                    <lsd-did-props id="didProperties" v-model="weenie.DidStats"></lsd-did-props>

                    <lsd-iid-props id="iidProperties" v-model="weenie.IidStats"></lsd-iid-props>

                    <lsd-bool-props id="boolProperties" v-model="weenie.BoolStats"></lsd-bool-props>

                    <lsd-positions id="positions" v-model="weenie.Positions"></lsd-positions>

                </div>
            </div>

            <lsd-item-spell-table v-model="weenie.Spells"></lsd-item-spell-table>
        </div>

        <div id="creature" class="tab-pane fade">
            <div v-if="weenie.ItemType==16">
            <lsd-panel title="Attributes">
                <lsd-attributes v-model="weenie.Attributes"></lsd-attributes>
            </lsd-panel>

            <lsd-skills
                v-model="weenie.Skills"
                :attributes="weenie.Attributes"
                @removed="weenie.Skills.splice($event, 1)">
            </lsd-skills>
            
            <lsd-body-part-list v-model="weenie.Body"></lsd-body-part-list>
            </div>
            <div v-else>Set Item Type to Creature</div>
        </div>

        <div id="book" class="tab-pane fade">
            <lsd-book v-if="weenie.WeenieTypeId==8" v-model="weenie.Book"></lsd-book>
            <div v-else>Set Weenie Type to Book to manage pages</div>
        </div>

        <div id="createlist" class="tab-pane fade">
            <lsd-create-items v-model="weenie.CreateList"></lsd-create-items>
        </div>

        <div id="generator" class="tab-pane fade">
            <lsd-generator :items="weenie.GeneratorTable"></lsd-generator>
        </div>

        <div id="emote" class="tab-pane fade">
            <lsd-emote-table v-model="weenie.EmoteTable"></lsd-emote-table>
        </div>

        <div id="history" class="tab-pane fade">
            <lsd-weenie-history v-model="weenie"></lsd-weenie-history>
        </div>
		
		<div id="appraise" class="modal" tabindex="-1" role="dialog">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<h2 class="modal-title"></h2>
			  </div>
			  <div class="modal-body"></div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>

        </div>

    </div>


    `
});

Vue.component('lsd-weenie-history', {
    props: ['weenie'],
    model: { prop: 'weenie', event: 'changed' },
    data() {
        return {};
    },
    methods: {
    },
    template: `
    <lsd-panel title="Change Log" stickHeader>
        <div class="row row-spacer">
            <div class="col-md-2">Changelog Entry</div>
            <div class="col-md-8"><textarea v-model="weenie.UserChangeSummary" class="form-control" rows="6" required></textarea></div>
        </div>

        <hr />

        <div v-for="entry in weenie.Changelog" class="row row-spacer">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ entry.Author }} on {{ entry.Created | toDate }}
                    </div>
                    <div class="panel-body text-prewrap">{{ entry.Comment }}</div>
                </div>
            </div>
        </div>

    </lsd-panel>
    `
});

Vue.component('lsd-weenie-import-dialog', {
    props: [ 'title' ],
    data() {
        return {
            importedWeenie: null
        };
    },
    methods: {
        show() {
            this.$refs.modal.show();
        },
        importWeenie() {
            if (this.importedWeenie) {
                var key = parseInt(this.importedWeenie);
                if (!key) return;
                // TODO: make this a service method
                var $this = this;
                $.getJSON("/Weenie/Get", { id: key },
                    function (res) {
                        if (res) {
                            $this.$emit('changed', res);
                        }
                    });
            }
        }
    },
    template: `
    <lsd-dialog ref="modal" :title="title" @saved="importWeenie">
        <input v-lsd-weenie-finder v-model="importedWeenie" type="text" class="form-control" placeholder="Weenie" />
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    </lsd-dialog>
    `
});
