﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-positions', {
    props: ['vals'],
    model: { prop: 'vals', event: 'changed' },
    methods: {
        addNew(key) {
            return {
                Key: key,
                Value: this.$weenie.newPosition()
            };
        }
    },
    template: `
    <lsd-props-base v-model="vals" name="Position" type="PositionType" :newfn="addNew">
        <input slot-scope="p" v-model="p.item.Value.Display" type="text" class="form-control" pattern="0x[0-9A-Fa-f]{8} \\[(-?\\d+\\.\\d+\\s?){3}\\] (-?\\d+\\.\\d+\\s?){4}" />
    </lsd-props-base>
    `
});
