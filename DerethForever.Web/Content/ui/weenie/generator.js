﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-generator', {
    props: ['items'],
    model: { prop: 'items', event: 'changed' },
    methods: {
        nodeMoved(evt) {
            var n = evt.el;
            var nn = evt.next;

            var current = -1;
            var before = -1;

            if (n && n.item) {
                current = this.items.indexOf(n.item);
            }

            if (nn && nn.item) {
                before = this.items.indexOf(nn.item);
            }

            if (current < before) before--;
            var item = this.items.splice(current, 1);
            this.items.splice(before, 0, ...item);

            this.$emit('changed', this.items);
        },
        deleteItem(idx) {
            this.items.splice(idx, 1);
            this.$emit('changed', this.items);
        },
        addNew() {
            if (!this.items) this.items = [];
            this.items.push(this.$weenie.newGeneratorListEntry());
        }
    },
    template: `
    <lsd-panel title="Generator Table" stickHeader showAdd @adding="addNew">
        <div v-lsd-drag-sort="'div.panel-default'" @moved="nodeMoved($event)" class="panel-group">
        <lsd-generator-item-entry v-for="(item, idx) in items" :key="$hash(item)" v-model="items[idx]" showDelete @deleted="deleteItem(idx)"></lsd-generator-item-entry>
        </div>
        <!-- footer -->
    </lsd-panel>
    `
});

Vue.component('lsd-generator-item-entry', {
    props: {
        'item': { type: Object },
        showDelete: { type: Boolean, default: false }
    },
    model: { prop: 'item', event: 'changed' },
    template: `
    <div class="panel-default panel-body form-group-sm border-after">
        <div class="row row-spacer">
            <div class="col-md-1">Slot</div>
            <div class="col-md-1"><input v-model="item.Slot" class="form-control" /></div>
            <div class="col-md-1">Probability</div>
            <div class="col-md-2"><input v-model="item.Probability" class="form-control" /></div>
            <div class="col-md-1">Delay</div>
            <div class="col-md-2"><input v-model="item.Delay" class="form-control" /></div>
            <div v-if="showDelete" class="col-md-offset-3 col-md-1"><button v-on:click.prevent="$emit('deleted', item)" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></button></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-1">WCID</div>
            <div class="col-md-3"><input v-lsd-weenie-finder v-model="item.WeenieClassId" class="form-control" /></div>
            <div class="col-md-1">Regen</div>
            <div class="col-md-3"><lsd-enum-select type="RegenerationType" v-model="item.WhenCreateEnum" keyOnly></lsd-enum-select></div>
            <div class="col-md-1">Location</div>
            <div class="col-md-3"><lsd-enum-select type="RegenerationLocation" v-model="item.WhereCreateEnum" keyOnly></lsd-enum-select></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-1">Initial</div>
            <div class="col-md-1"><input v-model="item.InitCreate" class="form-control" /></div>
            <div class="col-md-1">Max</div>
            <div class="col-md-1"><input v-model="item.MaxNumber" class="form-control" /></div>
            <div class="col-md-1">Palette</div>
            <div class="col-md-1"><input v-model="item.PaletteId" class="form-control" /></div>
            <div class="col-md-1">Shade</div>
            <div class="col-md-1"><input v-model="item.Shade" class="form-control" /></div>
            <div class="col-md-1">Stack</div>
            <div class="col-md-1"><input v-model="item.StackSize" class="form-control" /></div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-1">Cell</div>
            <div class="col-md-2"><input v-model="item.ObjectCell" class="form-control" /></div>
            <div class="col-md-1">Frame</div>
            <div class="col-md-8"><input v-model="item.Frame.Display" class="form-control" /></div>
        </div>
    </div>
    `
});
