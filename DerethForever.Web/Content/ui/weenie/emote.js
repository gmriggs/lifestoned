﻿
/// <reference path="../../../Scripts/vue.js" />

const EFM = {
    'Style': [5],
    'SubStyle': [5],
    'Quest': [12, 13, 22, 23, 30, 32, 38],
    'ClassId': [1, 6],
    'VendorType': [2],
    'MinHealth': [15],
    'MaxHealth': [15]
};

function showEmoteField(name, category) {
    var cat = EFM[name];
    if (cat)
        return cat.includes(category);
    return false;
}

const EAM = {
    'Message': [1, 8, 10, 13, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 30, 31, 32, 33, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 51, 58, 59, 60, 61, 64, 65, 67, 68, 70, 71, 75, 76, 79, 80, 81, 82, 83, 84, 85, 86, 88, 89, 102, 103, 104, 105, 106, 107, 108, 109, 114, 121],
    'Amount': [28, 29, 32, 33, 34, 47, 48, 53, 54, 55, 69, 70, 72, 84, 85, 86, 89, 90, 102, 103, 104, 105, 106, 107, 108, 109, 111, 119, 120],
    'Amount64': [2, 62, 112, 113],
    'Stat': [28, 29, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 50, 53, 54, 55, 69, 75, 110, 114, 115, 118],
    'Percent': [49, 50, 118],
    'Min': [30, 36, 39, 40, 41, 42, 43, 44, 50, 59, 71, 82],
    'Max': [30, 36, 39, 40, 41, 42, 43, 44, 50, 59, 71, 82],
    'HeroXp64': [2, 62, 112, 113],
    'WealthRating': [56],
    'TreasureClass': [56],
    'TreasureType': [56],
    'Motion': [5, 52],
    'MPosition': [63, 99, 100],
    'Frame': [4, 6, 11, 87],
    'SpellId': [14, 19, 27, 73],
    'PScript': [7],
    'Sound': [9],
    'TestString': [38, 75],
    'Minimum64': [49, 114],
    'Maximum64': [49, 114],
    'FMin': [37],
    'FMax': [37],
    'Display': [49, 50],
    'Item': [3, 74, 76]
};

function showActionField(name, type) {
    var cat = EAM[name];
    if (cat/* && name !== 'Item'*/)
        return cat.includes(type);
    return false;
}

Vue.component('lsd-emote-category-label', {
    props: ['category'],
    data() {
        return {
            categoryTitle: ''
        };
    },
    mounted() {
        var $this = this;
        getEnumValue('EmoteCategory', this.category).then(function (val) {
            $this.categoryTitle = val;
        });
    },
    template: `
    <span :data-category="category">{{categoryTitle}}</span>
    `
});

Vue.component('lsd-emote-table', {
    props: ['emotes'],
    model: { prop: 'emotes', event: 'changed' },
    data() {
        return {
            selectedCategory: 0,
            newCategory: null,
            expanded: false
        };
    },
    computed: {
        emoteTable: function () {
            return this.emotes ? this.emotes : [];
        },
        currentSet: function () {
            return this.emoteTable.length > 0 ? this.emoteTable[this.selectedCategory].Emotes : [];
        },
        currentCategory: function () {
            return this.emoteTable.length > 0 ? this.emoteTable[this.selectedCategory] : null;
        }
    },
    methods: {
        fullscreen() {
            this.$el.requestFullscreen();
        },
        openDialog() {
            this.$refs.modal.show();
        },
        addNew() {
            if (!this.emotes) this.emotes = [];
            if (this.newCategory) {
                var key = parseInt(this.newCategory);
                var table = this.emoteTable.find(i => i.EmoteCategoryId === key);
                if (table) {
                    table.Emotes.push(this.$weenie.newEmote(key));
                } else {
                    this.emoteTable.push(this.$weenie.newEmoteCategory(key));
                }
                this.$emit('changed', this.emotes);
            }
        },
        deleted(emote) {
            var idx = this.currentSet.indexOf(emote);
            if (idx >= 0) {
                this.currentSet.splice(idx, 1);
            }
        },
        openImport() {
            this.$refs.import.show();
        },
        importWeenie(weenie) {
            if (weenie && weenie.EmoteTable) {
                this.$emit('changed', weenie.EmoteTable);
            }
        },
        expandAll() {
            $(this.$el).find('.collapse').collapse(this.expanded ? 'hide' : 'show');
            this.expanded = !this.expanded;
        }
    },
    template: `
    <div class="panel panel-default">
        <div class="panel-heading clearfix stick-scroll">
            <h3 class="panel-title pull-left">Emotes</h3>
            <div class="pull-right">
                <select v-model="selectedCategory">
                    <option v-for="(category, idx) in emoteTable" :value="idx" :selected="idx == selectedCategory"><lsd-emote-category-label :category="category.EmoteCategoryId"/></option>
                </select>
                <button v-if="!expanded" @click="expandAll" type="button" class="btn btn-xs btn-default" title="Expand All"><i class="glyphicon glyphicon-resize-full"></i></button>
                <button v-else @click="expandAll" type="button" class="btn btn-xs btn-default" title="Collapse All"><i class="glyphicon glyphicon-resize-small"></i></button>
                <button @click="openImport" type="button" class="btn btn-xs btn-default" title="Import"><i class="glyphicon glyphicon-import"></i></button>
                <button @click="openDialog" type="button" class="btn btn-xs btn-default" title="Add"><i class="glyphicon glyphicon-plus"></i></button>
                <!-- button type="button" @click="fullscreen()" class="btn btn-default"><i class="glyphicon glyphicon-fullscreen"></i></button -->
            </div>
        </div>
        <div class="panel-body panel-group">
            <!-- lsd-emote-category :category="currentCategory"></lsd-emote-category -->

            <lsd-emote-set v-for="(emote, idx) in currentSet" :key="$hash(emote)" v-model="currentSet[idx]" @deleted="deleted"></lsd-emote-set>
        </div>

        <lsd-dialog ref="modal" title="Add Emote Set" @saved="addNew">
            <lsd-enum-select type="EmoteCategory" v-model="newCategory" keyOnly></lsd-enum-select>
        </lsd-dialog>
        <lsd-weenie-import-dialog ref="import" title="Import Emote Table" @changed="importWeenie"></lsd-weenie-import-dialog>
    </div>
    `
});

Vue.component('lsd-emote-category', {
    props: ['category'],
    data() {
        return {
            categoryTitle: ''
        };
    },
    mounted() {
        var $this = this;
        getEnumValue('EmoteCategory', this.category.EmoteCategoryId).then(function (val) {
            $this.categoryTitle = val;
        });
    },
    template: `
    <lsd-panel-collapse :title="categoryTitle">
        <lsd-emote-set v-for="emote in category.Emotes" key="$hash(emote)" v-model="emote"></lsd-emote-set>
    </lsd-panel-collapse>
    `
});

Vue.component('lsd-emote-set', {
    props: ['emote'],
    model: { prop: 'emote', event: 'changed' },
    data() {
        return {
            newAction: null
        };
    },
    methods: {
        showField(name) {
            return showEmoteField(name, this.emote.Category);
        },
        nodeMoved(evt) {
            var n = evt.el;
            var nn = evt.next;

            var current = -1;
            var before = -1;

            if (n && n.action) {
                current = this.emote.Actions.indexOf(n.action);
            }

            if (nn && nn.action) {
                before = this.emote.Actions.indexOf(nn.action);
            }

            if (current < before) before--;
            var item = this.emote.Actions.splice(current, 1);
            this.emote.Actions.splice(before, 0, ...item);

            this.$emit('changed', this.emote);
        },
        deleted() {
            this.$emit('deleted', this.emote);
        },
        actionDeleted(action) {
            var idx = this.emote.Actions.indexOf(action);
            if (idx >= 0) {
                this.emote.Actions.splice(idx, 1);
            }
        },
        openDialog() {
            this.$refs.modal.show();
        },
        addNew() {
            if (this.newAction) {
                var key = parseInt(this.newAction);
                this.emote.Actions.push(this.$weenie.newEmoteAction(key));
            }
        }
    },
    mounted() {
    },
    template: `
    <div class="panel-default panel-body border-after">
        <div class="row-spacer clearfix">
            <div class="col-md-3">Probability</div>
            <div class="col-md-3"><input v-model="emote.Probability" type="text" class="form-control" /></div>
            <div class="col-md-5"></div>
            <div class="col-md-1">
                <button @click="deleted" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </div>

        <div v-if="showField('Quest')" class="row row-spacer">
            <div class="col-md-3">Quest</div>
            <div class="col-md-3">
            <input v-model="emote.Quest" type="text" class="form-control" />
            </div>
        </div>

        <div v-if="showField('Style')" class="row row-spacer">
            <div class="col-md-3">Style</div>
            <div class="col-md-3">
            <lsd-enum-select type="MotionCommand" v-model="emote.Style" :mask="0x80000000" keyOnly></lsd-enum-select>
            </div>
        </div>

        <div v-if="showField('SubStyle')" class="row row-spacer">
            <div class="col-md-3">Sub-Style</div>
            <div class="col-md-3">
            <lsd-enum-select type="MotionCommand" v-model="emote.SubStyle" :mask="0x40000000" keyOnly></lsd-enum-select>
            </div>
        </div>

        <div v-if="showField('ClassId')" class="row row-spacer">
            <div class="col-md-3">ClassId</div>
            <div class="col-md-3">
            <input v-lsd-weenie-finder v-model="emote.ClassId" type="text" class="form-control weenieClassEntry weenie-select" />
            </div>
        </div>

        <div v-if="showField('MaxHealth')" class="row row-spacer">
            <div class="col-md-3">Max Health</div>
            <div class="col-md-3">
            <input v-model="emote.MaxHealth" type="text" class="form-control" />
            </div>
        </div>

        <div v-if="showField('MinHealth')" class="row row-spacer">
            <div class="col-md-3">MinHealth</div>
            <div class="col-md-3">
            <input v-model="emote.MinHealth" type="text" class="form-control" />
            </div>
        </div>

        <div v-if="showField('VendorType')" class="row row-spacer">
            <div class="col-md-3">Vendor Type</div>
            <div class="col-md-3">
            <lsd-enum-select type="VendorEmoteType" v-model="emote.VendorType" keyOnly></lsd-enum-select>
            </div>
        </div>

        <lsd-panel-collapse title="Actions" showAdd @adding="openDialog">
            <div v-lsd-drag-sort="'div.panel'" v-on:moved="nodeMoved($event)" class="panel-body panel-group">
            <lsd-emote-action v-for="(action, idx) in emote.Actions" :key="$hash(action)" v-model="emote.Actions[idx]" @deleted="actionDeleted"></lsd-emote-action>
            </div>
        </lsd-panel-collapse>

        <lsd-dialog ref="modal" title="Add Emote Action" @saved="addNew">
            <lsd-enum-select type="EmoteType" v-model="newAction" keyOnly></lsd-enum-select>
        </lsd-dialog>
    </div>
    `
});

Vue.component('lsd-emote-action', {
    props: ['action'],
    model: { prop: 'action', event: 'changed' },
    data() {
        return {
            actionTitle: ''
        };
    },
    methods: {
        showField(name) {
            return showActionField(name, this.action.EmoteActionType);
        },
        deleted() {
            this.$emit('deleted', this.action);
        }
    },
    mounted() {
        var $this = this;
        getEnumValue('EmoteType', this.action.EmoteActionType).then(function (val) {
            $this.actionTitle = val;
        });
    },
    template: `
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <a class="col-md-6" >{{actionTitle}}</a>
            <div class="col-md-offset-5 col-md-1">
                <button @click="deleted" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </div>
        <div class="panel-body">
            <div class="row row-spacer">
                <div class=" col-md-2">Delay</div>
                <div class=" col-md-2">Extent</div>
            </div>
            <div class="row row-spacer">
                <div class=" col-md-2"><input v-model="action.Delay" type="text" class="form-control" />
                </div>
                <div class=" col-md-2"><input v-model="action.Extent" type="text" class="form-control" />
                </div>
            </div>

            <div v-if="showField('Message')" class="row row-spacer">
                <div class="col-md-2">Message</div>
                <div class="col-md-8"><textarea v-model="action.Message" class="form-control wide"></textarea></div>
            </div>

            <div v-if="showField('Amount')" class="row row-spacer">
                <div class="col-md-2">Amount</div>
                <div class="col-md-3"><input v-model="action.Amount" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Amount64')" class="row row-spacer">
                <div class="col-md-2">Amount64</div>
                <div class="col-md-3"><input v-model="action.Amount64" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Stat')" class="row row-spacer">
                <div class="col-md-2">Stat</div>
                <div class="col-md-3"><input v-model="action.Stat" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Percent')" class="row row-spacer">
                <div class="col-md-2">Percent</div>
                <div class="col-md-3"><input v-model="action.Percent" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Min')" class="row row-spacer">
                <div class="col-md-2">Min</div>
                <div class="col-md-3"><input v-model="action.Min" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Max')" class="row row-spacer">
                <div class="col-md-2">Max</div>
                <div class="col-md-3"><input v-model="action.Max" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('HeroXp64')" class="row row-spacer">
                <div class="col-md-2">HeroXp64</div>
                <div class="col-md-3"><input v-model="action.HeroXp64" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('WealthRating')" class="row row-spacer">
                <div class="col-md-2">Wealth Rating</div>
                <div class="col-md-3">
                <lsd-enum-select type="WealthRating" v-model="action.WealthRating" keyOnly></lsd-enum-select>
                </div>
            </div>

            <div v-if="showField('TreasureClass')" class="row row-spacer">
                <div class="col-md-2">Treasure Class</div>
                <div class="col-md-3">
                <lsd-enum-select type="TreasureClass" v-model="action.TreasureClass" keyOnly></lsd-enum-select>
                </div>
            </div>

            <div v-if="showField('TreasureType')" class="row row-spacer">
                <div class="col-md-2">Treasure Type</div>
                <div class="col-md-3"><input v-model="action.TreasureType" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Motion')" class="row row-spacer">
                <div class="col-md-2">Motion</div>
                <div class="col-md-3">
                <lsd-enum-select type="MotionCommand" v-model="action.Motion" :mask="0x50000000" keyOnly></lsd-enum-select>
                </div>
            </div>

            <div v-if="showField('MPosition')" class="row row-spacer">
                <div class="col-md-2">Position</div>
                <div class="col-md-3"><input v-model="action.MPosition.Display" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Frame')" class="row row-spacer">
                <div class="col-md-2">Frame</div>
                <div class="col-md-3"><input v-model="action.Frame.Display" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('SpellId')" class="row row-spacer">
                <div class="col-md-2">Spell</div>
                <div class="col-md-3"><input v-model="action.SpellId" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('PScript')" class="row row-spacer">
                <div class="col-md-2">Physics Script</div>
                <div class="col-md-3">
                <lsd-enum-select type="PhysicsScriptType" v-model="action.PScript" keyOnly></lsd-enum-select>
                </div>
            </div>

            <div v-if="showField('Sound')" class="row row-spacer">
                <div class="col-md-2">Sound</div>
                <div class="col-md-3"><input v-model="action.Sound" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('TestString')" class="row row-spacer">
                <div class="col-md-2">Test String</div>
                <div class="col-md-3"><input v-model="action.TestString" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Minimum64')" class="row row-spacer">
                <div class="col-md-2">Minimum64</div>
                <div class="col-md-3"><input v-model="action.Minimum64" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Maximum64')" class="row row-spacer">
                <div class="col-md-2">Maximum64</div>
                <div class="col-md-3"><input v-model="action.Maximum64" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('FMin')" class="row row-spacer">
                <div class="col-md-2">FMin</div>
                <div class="col-md-3"><input v-model="action.FMin" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('FMax')" class="row row-spacer">
                <div class="col-md-2">FMax</div>
                <div class="col-md-3"><input v-model="action.FMax" type="text" class="form-control" /></div>
            </div>

            <div v-if="showField('Display')" class="row row-spacer">
                <div class="col-md-2">Display</div>
                <div class="col-md-1"><input v-model="action.Display" type="checkbox" class="form-control" /></div>
            </div>

            <lsd-create-items-header v-if="showField('Item')"></lsd-create-items-header>
            <lsd-create-items-entry v-if="showField('Item')" v-model="action.Item"></lsd-create-items-entry>

        </div>
    </div>
    `
});

//<div v-if="showField('')" class="row row-spacer">
//    <div class="col-md-2"></div>
//    <div class="col-md-3"><input v-model="action." type="text" class="form-control" /></div>
//</div>
