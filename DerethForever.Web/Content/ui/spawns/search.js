﻿/// <reference path="../../../Scripts/jquery-3.2.1.js" />
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-spawn-list', {
    props: [ 'results' ],
    methods: {
    },
    created() {

    },
    mounted() {
    },
    template: `
    <div>
        <table class="table table-bordered table-striped table-hover" v-if="results.length > 0">
        <thead>
            <tr>
                <th>Id</th><th>Description</th><th>Last Modified</th><th>Modified By</th><th></th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="item in results" @click="$emit('changed', item)">
                <td>{{ item.Key.toHexStr() }}</td><td>{{ item.Description }}</td><td>{{ item.LastModified }}</td><td>{{ item.ModifiedBy }}</td><td>...</td>
            </tr>
        </tbody>
        </table>
    </div>
    `
});