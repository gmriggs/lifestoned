﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-panel', {
    props: {
        title: { type: String },
        showDelete: { type: Boolean, default: false },
        showFooter: { type: Boolean, default: false },
        showAdd: { type: Boolean, default: false },
        stickHeader: { type: Boolean, default: false }
    },
    template: `
    <div class="panel panel-default">
        <div class="panel-heading clearfix" :class="{ 'stick-scroll' : stickHeader }">
            <h3 class="panel-title col-md-6"><a>{{title}}</a></h3>
            <slot name="headerCommands">
            <div v-if="showAdd" class="col-md-offset-5 col-md-1">
                <button @click="$emit('adding')" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button>
            </div>
            <div v-else-if="showDelete" class="col-md-offset-5 col-md-1">
                <button @click="$emit('deleting')" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
            </slot>
        </div>
        <div class="panel-body">
            <slot></slot>
        </div>
        <div v-if="showFooter" class="panel-footer">
        <slot name="footer"></slot>
        </div>
    </div>
    `
});

Vue.component('lsd-panel-collapse', {
    props: {
        title: { type: String },
        showDelete: { type: Boolean, default: false },
        showFooter: { type: Boolean, default: false },
        showAdd: { type: Boolean, default: false }
    },
    computed: {
        panelId() {
            return `p_${this._uid}`;
        }
    },
    template: `
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <a data-toggle="collapse" class="col-md-6" :href="'#' + panelId" >{{title}}</a>
            <div v-if="showAdd" class="col-md-offset-5 col-md-1">
            <slot name="adder">
                <button @click="$emit('adding')" type="button" class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button>
            </slot>
            </div>
            <div v-else-if="showDelete" class="col-md-offset-5 col-md-1">
                <button @click="$emit('deleting')" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </div>
        <div :id="panelId" class="panel-collapse collapse">
            <slot></slot>
        </div>
        <div v-if="showFooter" class="panel-footer">
        <slot name="footer"></slot>
        </div>
    </div>
    `
});
