﻿
/// <reference path="../../../Scripts/jquery-3.2.1.js" />
/// <reference path="../../../Scripts/vue.js" />

let enum_cache = {};
function getEnum(name) {
    var e = enum_cache[name];
    if (e === undefined) {
        e = localStorage.getItem(name);
        if (e) {
            e = JSON.parse(e);
            enum_cache[name] = e;
        }
    }

    if (e === undefined || e === null) {
        var pget = new Promise(function (resolve, reject) {
            $.getJSON("/Enum/GetValues", { name: name },
                function (res) {
                    enum_cache[name] = res;
                    localStorage.setItem(name, JSON.stringify(res));
                    resolve(res);
                })
                .fail(function () {
                    reject();
                });
        });

        enum_cache[name] = pget;
        return pget;
    } else if (e instanceof Promise) {
        return e;
    } else {
        return new Promise(function (resolve, reject) {
            resolve(e);
        });
    }
}

function getEnumValue(name, value) {
    return new Promise(function (resolve, reject) {
        getEnum(name)
            .then(function (vals) {
                for (let i in vals) {
                    let kv = vals[i];
                    if (kv.Key === value) {
                        resolve(kv.Value);
                        break;
                    }
                }
                reject();
            })
            .catch(function () {
                reject();
            });
    });
}

Vue.component('lsd-enum-select', {
    props: {
        type: { type: String },
        selected: { type: [Number, Object], default: null },
        keyOnly: { type: Boolean, default: false },
        mask: { type: Number, default: 0 }
    },
    model: { prop: 'selected', event: 'changed' },
    data() {
        return { values: [] };
    },
    methods: {
        selectionChanged(event) {
            //console.log('selectionChanged', this.selected, event);
            // bindings are one-way (down)
            // find the values (jquery-way, figure out vue-way)
            // raise event to let our parent know about the change
            var o = $(event.target.selectedOptions[0]);
            console.log(o);
            var s;
            if (this.keyOnly) {
                s = parseInt(o.val());
            } else {
                s = {
                    Key: parseInt(o.val()),
                    Value: o.text()
                };
            }
            this.$emit('changed', s);
        }
    },
    created() {
        var $this = this;

        getEnum($this.type).then(function (vals) {
            $this.values = vals;
        });
    },
    template: `
    <select class="form-control" :value="selected" @change="selectionChanged">
        <option v-for="value in values" v-if="mask === 0 || ((parseInt(value.Key) >>> 0) & (mask >>> 0)) !== 0" :value="value.Key" :selected="selected==value.Key">{{ value.Value }}</option>
    </select>
    `
});

Vue.component('lsd-flags-select', {
    props: ['type', 'selected'],
    model: { prop: 'selected', event: 'changed' },
    data() {
        return { values: [], c_selected: false };
    },
    created() {
        var $this = this;

        this.c_selected = this.selected;

        getEnum($this.type).then(function (vals) {
            $this.values = vals;
            $this.$el.selectize.addOption($this.values);
            $this.$el.selectize.refreshOptions();
            $this.c_selected = $this.selected;

            $this.values.forEach(function (item) {
                if ((item.Key & $this.c_selected) != 0) {
                    $this.$el.selectize.addItem(item.Key, true);
                }
            });
        });

        //$.getJSON("/Enum/GetValues", { name: $this.type },
        //    function (res) {
        //        $this.values = res;
        //        $this.$el.selectize.addOption($this.values);
        //        $this.$el.selectize.refreshOptions();

        //        $this.values.forEach(function (item) {
        //            if ((item.Key & $this._selected) != 0) {
        //                $this.$el.selectize.addItem(item.Key, true);
        //            }
        //        });
        //    });
    },
    mounted() {
        var $this = this;

        $(this.$el).selectize({
            maxItems: null,
            allowEmptyOption: true,
            valueField: 'Key',
            labelField: 'Value',
            onChange: function (value) {
                var flags = 0;
                value.forEach(function (i) { flags |= i; });
                $this.$emit('changed', flags);
            }
        });
    },
    destroyed() {
        if (this.$el.selectize) {
            this.$el.selectize.destroy();
        }
    },
    template: `
    <select class="form-control" multiple>
        <!-- option v-for="value in values" :value="value.Key" :label="value.Value" :selected="(c_selected & value.Key) != 0">{{ value.Value }}</option -->
    </select>
    `
});
/*
    <div style="display:flex; flex-flow:row wrap; max-height:30rem;">
        <label v-for="value in values" style="flex:1 0;white-space:nowrap;">
            <input type="checkbox" :value="value.Key" :checked="(_selected & value.Key) != 0"/>
            {{ value.Value }}
        </label>
    </div>
*/