﻿
/// <reference path="../../../Scripts/vue.js" />

function mapWatchers(source, propName, eventName) {
    if (!source || !source.$options || !source.$options.propsData) return;

    var value = source.$options.propsData[propName];
    if (!value) return;

    for (var key of Object.keys(value)) {
        var watchKey = `${propName}.${key}`;
        source.$watch(watchKey, function (nval, oval) {
            source.$emit(eventName, source.$options.propsData[propName]);
        });
    }
}

const AutoWatchMixin = {
    install(Vue, options) {
        Vue.mixin({
            created() {
                if (this.$options.model) {
                    mapWatchers(this, this.$options.model.prop, this.$options.model.event);
                }
            }
        });
    }
};

const HashCodeMixin = {
    install(Vue, options) {
        Vue.mixin({
            methods: {
                $hash(item) {
                    this.$hash.count = this.$hash.count || 1;
                    if (!item._hashCode) item._hashCode = this.$hash.count++;
                    return item._hashCode;
                }
            }
        });
    }
};
Vue.use(HashCodeMixin);

var emit = (vnode, name, data) => {
    var handlers = (vnode.data && vnode.data.on) ||
        (vnode.componentOptions && vnode.componentOptions.listeners);

    if (handlers && handlers[name]) {
        handlers[name].fns(data);
    }
};

// https://www.jqueryscript.net/other/Drag-To-Sort-Plugin-jQuery.html
Vue.directive('lsd-drag-sort', {
    bind: function (el, binding, vnode) {
        // applied to the 'root' of the items to be sorted

        // just use the jquery version for now
        const replaceStyle = {
            'background-color': '#797979',
            'border': '1px dashed #ddd'
        };
        const dragStyle = {
            'position': 'fixed',
            'box-shadow': '10px 10px 20px 0 #eee'
        };

        var thisEle = $(el);
        thisEle.on('mousedown.dragSort', binding.value, function (event) {

            var selfEle = $(this);

            if (event.which !== 1) {
                return;
            }

            var tagName = event.target.tagName.toUpperCase();
            if (tagName === 'INPUT' || tagName === 'TEXTAREA' || tagName === 'SELECT') {
                return;
            }

            var moveEle = $(this);

            var offset = selfEle.offset();
            var rangeX = event.pageX - offset.left;
            var rangeY = event.pageY - offset.top;

            var replaceEle = selfEle.clone()
                .css('height', selfEle.outerHeight())
                .css(replaceStyle)
                .empty();
            dragStyle.width = selfEle.width();
            var move = true;

            $(document).on('mousemove.dragSort', function (event) {
                if (move) {
                    moveEle.before(replaceEle).css(dragStyle).appendTo(moveEle.parent());
                    move = false;
                }

                var thisOuterHeight = moveEle.outerHeight();

                var scrollTop = $(document).scrollTop();
                var scrollLeft = $(document).scrollLeft();

                var moveLeft = event.pageX - rangeX - scrollLeft;
                var moveTop = event.pageY - rangeY - scrollTop;

                var prevEle = replaceEle.prev();
                var nextEle = replaceEle.next().not(moveEle);

                moveEle.css({
                    left: moveLeft,
                    top: moveTop
                });

                if (prevEle.length > 0 && moveTop + scrollTop < prevEle.offset().top + prevEle.outerHeight() / 2) {
                    replaceEle.after(prevEle);
                } else if (nextEle.length > 0 && moveTop + scrollTop > nextEle.offset().top - nextEle.outerHeight() / 2) {
                    replaceEle.before(nextEle);
                }
            });

            $(document).on('mouseup.dragSort', function (event) {
                $(document).off('mousemove.dragSort mouseup.dragSort');
                if (!move) {
                    replaceEle.before(moveEle.removeAttr('style')).remove();

                    var moveNode = moveEle[0].__vue__;
                    var nextNode = null;

                    var nn = moveEle.next();
                    if (nn.length > 0) {
                        nextNode = nn[0].__vue__;
                    }
                    emit(vnode, 'moved', { el: moveNode, next: nextNode });
                }
            });
        });

    }
});

///////////////////////////////////////////////////////////////////////////////

function displayWeenie(item) {
    return `(${item.WeenieClassId}) ${item.Name}`;
}

function findWeenie(data, callback) {
    $.post('/Weenie/WeenieFinder', data,
        function (data, status, xhr) {
            callback(data, status, xhr);
        }
    )
    .fail(function (xhr, status) {
        callback(null, status, xhr);
    });
}

function doFindWeenie(display, id) {
    if (id || id === 0) {
        findWeenie({ WeenieClassId: id },
            function (data, status, xhr) {
                if (data && data.length > 0) {
                    display.val(displayWeenie(data[0]));
                } else {
                    display.val(id);
                }
            });
    }
}

Vue.directive('lsd-weenie-finder', {
    bind: function (el, binding, vnode) {
    },
    inserted: function (el, binding, vnode) {
        var $el = $(el);

        if ($el.prop('type') === 'hidden' || $el.data('provide') === 'typeahead')
            // already set up
            return;

        var display = $el.clone();
        $el.prop('type', 'hidden');
        display.removeAttr('id').removeAttr('name').addClass('typeahead');
        display.prop('autocomplete', 'off').data('provide', 'typeahead');
        display.insertBefore($el);

        doFindWeenie(display, el.value);

        display.typeahead({
            items: 'all',
            minLenght: 3,
            appendTo: null,
            fitToElement: true,
            displayText: function (item) {
                return displayWeenie(item);
            },
            afterSelect: function (item) {
                $el.val(item ? item.WeenieClassId : 0);
                el.dispatchEvent(new Event('input'));
            },
            source: function (val, cb) {
                var query;
                var num = Number(val);
                if (!isNaN(num)) query = { WeenieClassId: num };
                else query = { PartialName: val };
                findWeenie(query,
                    function (data, status, xhr) {
                        cb(data);
                    }
                );
            }
        });
    },
    update: function (el, binding, vnode, oldvnode) {
        if (vnode === oldvnode) return;

        var display = $(el).prev();
        doFindWeenie(display, el.value);
    }
});

Vue.directive('lsd-weenie-display', {
    bind: function (el, binding, vnode) {
        var $el = $(el);

        if ($el.data('wcid'))
            // already set up
            return;

        var id = $el.text();
        if (id) {
            findWeenie({ WeenieClassId: id },
                function (data, status, xhr) {
                    if (data && data.length > 0) {
                        $el.text(displayWeenie(data[0]));
                        $el.data('wcid', id);
                    }
                });
        }
    }
});

Vue.directive('lsd-enum-display', {
    bind: function (el, binding, vnode) {
        var key = binding.value.key;
        var type = binding.value.type;

        if (!type || key === null || key === undefined)
            return;

        var $el = $(el);
        getEnumValue(type, key).then(function (value) {
            $el.text(value);
        });
    },
    update: function (el, binding, vnode, oldvnode) {
        if (vnode === oldvnode) return;

        var key = binding.value.key;
        var type = binding.value.type;

        if (!type || key === null || key === undefined)
            return;

        var $el = $(el);
        getEnumValue(type, key).then(function (value) {
            $el.text(value);
        });
    }
});

Vue.directive('selectize', {
    bind: function (el, binding, vnode) {
    },
    inserted: function (el, binding, vnode) {
        //console.log('insert', el);
        //var $this = this;

        $(el).selectize({
            allowEmptyOption: true
        });

        //$(el).selectize({
        //    maxItems: null,
        //    allowEmptyOption: true,
        //    valueField: 'Key',
        //    labelField: 'Value',
        //    //onChange: function (value) {
        //    //    var flags = 0;
        //    //    value.forEach(function (i) { flags |= i; });
        //    //    $this.$emit('changed', flags);
        //    //}
        //});
    },
    update: function (el, binding, vnode, oldvnode) {
        if (vnode === oldvnode) return;

        //console.log('update', el);

        //if (el.selectize) {
        //    el.selectize.destroy();
        //}
        //$(el).selectize({});

        //$(el).selectize({
        //    maxItems: null,
        //    allowEmptyOption: false,
        //    valueField: 'Key',
        //    labelField: 'Value',
        //    //onChange: function (value) {
        //    //    var flags = 0;
        //    //    value.forEach(function (i) { flags |= i; });
        //    //    $this.$emit('changed', flags);
        //    //}
        //});

    },
    unbind: function (el, binding, vnode) {
        if (el.selectize) {
            el.selectize.destroy();
        }
    }
});
